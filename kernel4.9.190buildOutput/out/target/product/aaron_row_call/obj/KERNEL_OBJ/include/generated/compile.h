/* This file is auto generated, version 3 */
/* SMP PREEMPT */
#define UTS_MACHINE "aarch64"
#define UTS_VERSION "#3 SMP PREEMPT Thu Sep 23 11:05:26 UTC 2021"
#define LINUX_COMPILE_BY "gitpod"
#define LINUX_COMPILE_HOST "ws-6c66ab4f-fb42-4a8b-9e4b-945daa5e2072"
#define LINUX_COMPILER "Android (4691093 based on r316199) clang version 6.0.2 (https://android.googlesource.com/toolchain/clang 183abd29fc496f55536e7d904e0abae47888fc7f) (https://android.googlesource.com/toolchain/llvm 34361f192e41ed6e4e8f9aca80a4ea7e9856f327) (based on LLVM 6.0.2svn)"
