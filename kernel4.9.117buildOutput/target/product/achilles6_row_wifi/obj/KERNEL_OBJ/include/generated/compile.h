/* This file is auto generated, version 5 */
/* SMP PREEMPT */
#define UTS_MACHINE "aarch64"
#define UTS_VERSION "#5 SMP PREEMPT Wed Sep 22 05:32:35 UTC 2021"
#define LINUX_COMPILE_BY "gitpod"
#define LINUX_COMPILE_HOST "ws-2e205637-621b-4339-90d3-6c05a963f1c4"
#define LINUX_COMPILER "Android (4691093 based on r316199) clang version 6.0.2 (https://android.googlesource.com/toolchain/clang 183abd29fc496f55536e7d904e0abae47888fc7f) (https://android.googlesource.com/toolchain/llvm 34361f192e41ed6e4e8f9aca80a4ea7e9856f327) (based on LLVM 6.0.2svn)"
