#!/bin/bash
 
me=$(readlink -f $0)
mydir=$(dirname $me)
 
mkdir -p $mydir/out/target/product/generic/obj/kernel
 
kernel_out_dir=$mydir/out/target/product/generic/obj/kernel
 
cross=$mydir/prebuilts/gcc/linux-x86/aarch64/aarch64-linux-android-4.9/bin/aarch64-linux-android-
 
make -j8 -C kernel KBUILD_RELSRC=$mydir/kernel O=$kernel_out_dir ARCH=arm64 CROSS_COMPILE=$cross KBUILD_BUILD_USER= KBUILD_BUILD_HOST= KCFLAGS=-mno-android AK57_defconfig
 
make -j8 -C kernel KBUILD_RELSRC=$mydir/kernel O=$kernel_out_dir ARCH=arm64 CROSS_COMPILE=$cross KBUILD_BUILD_USER= KBUILD_BUILD_HOST= KCFLAGS=-mno-android
 
make -j8 -C kernel KBUILD_RELSRC=$mydir/kernel O=$kernel_out_dir ARCH=arm64 CROSS_COMPILE=$cross KBUILD_BUILD_USER= KBUILD_BUILD_HOST= KCFLAGS=-mno-android dtbs
 
make -j8 -C kernel KBUILD_RELSRC=$mydir/kernel O=$kernel_out_dir ARCH=arm64 CROSS_COMPILE=$cross KBUILD_BUILD_USER= KBUILD_BUILD_HOST= KCFLAGS=-mno-android modules
 
make -j8 -C kernel KBUILD_RELSRC=$mydir/kernel O=$kernel_out_dir INSTALL_MOD_PATH=$mydir/kernel/out/target/product/generic INSTALL_MOD_STRIP="--strip-debug --remove-section=.note.gnu.build-id" ARCH=arm64 CROSS_COMPILE=$cross KBUILD_BUILD_USER= KBUILD_BUILD_HOST= modules_install
